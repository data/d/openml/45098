# OpenML dataset: Ovarian

https://www.openml.org/d/45098

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Ovarian dataset**

**Authors**: E. Petricoin, A. Ardekani, B. Hitt, P. Levine, V. Fusaro, S. Steinberg, G. Mills, C. Simone, D. Fishman, E. Kohn, et al

**Please cite**: ([URL](https://www.sciencedirect.com/science/article/abs/pii/S0140673602077462)): E. Petricoin, A. Ardekani, B. Hitt, P. Levine, V. Fusaro, S. Steinberg, G. Mills, C. Simone, D. Fishman, E. Kohn, et al, Use of proteomic patterns in serum to identify ovarian cancer, Lancet 359 (9306) (2002) 572-577

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45098) of an [OpenML dataset](https://www.openml.org/d/45098). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45098/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45098/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45098/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

